'''
Работаем с битами аудиофайла
Бит гуляет из уха в уха, то есть например
Первый удар бита полнстью в правое ухо, второй в левое и тд
Все остальные дорожки в обычном стерео

Итак, результаты неутешительные
Биты вышли гуляющими, я поразвлекался, можно ускорять и замедлять песню в разу, беря слайсы песни разным образом
Но на выходе звук в любом случае получается рваный, после каждого бита он на доли секунды выключается и
Снова появляется, но теперь в стандартном стерео (или в другом ухе)
Я не знаю с чем это может быть связано, возможно с тем, что я слишком резко перекидываю звук в разные уши
Ибо когда я делал walking sound там все было четко
Возможно это связано с ограничениями библиотеки и ffmpeg, тогда нужны какие-то более профессиональные решения
'''

import aubio
import librosa
# import numpy as np
from pydub import AudioSegment
from random import choice

song, sr = librosa.load('main.mp3')
tempo, beats = librosa.beat.beat_track(y=song, sr=sr)  # считаем BPM песни
print(tempo)
beats = list(librosa.frames_to_time(beats, sr=sr))
del song, sr

pan = 50  # кол-во мс, время на которое мы посылаем сигнал в 1 ухо
right_ear = True  # посылаем сигнал в правое ухо или левое
song = AudioSegment.from_file('main.mp3', format='mp3')
for i in range(len(beats) - 1):
    '''if right_ear:
        song = song[:beats[i] * 1000 + 10] + song[beats[i] * 1000:beats[i] * 1000 + pan].pan(+0.8) + song[beats[i] * 1000 + pan - 10:]
        right_ear = False
    else:
        song = song[:beats[i] * 1000 + 10] + song[beats[i] * 1000:beats[i] * 1000 + pan].pan(-0.8) + song[beats[i] * 1000 + pan - 10:]
        right_ear = True  # тут собственно мы берем слайс песни до текущего бита
        # текущий бит посылаем в какое-то ухо и также берем слайс конца песни с текущего бита + 1'''

    # А тут я просто баловался и решил на рандоме каждый бит в какое-то ухо кидать, как получится
    if choice([True, False]): song = song[:beats[i] * 1000 + 10] + song[beats[i] * 1000:beats[i] * 1000 + pan].pan(+0.8) + song[beats[i] * 1000 + pan - 10:]
    else: song = song[:beats[i] * 1000 + 10] + song[beats[i] * 1000:beats[i] * 1000 + pan].pan(-0.8) + song[beats[i] * 1000 + pan - 10:]

file = song.export('walking beat.wav', format='wav')
print('done')
