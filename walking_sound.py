"""Эта уже другая попытка сделать 8д, тоже не совсем удачная
Но к своей цели я стал ближе
Звук переходит из уха в ухо, можно настраивать скорость перехода
В настоящем 8д ощущение, что звук движется не просто от уха к уху, а по окружности
Он движется между ушами через верх, то есть музыка играет откуда-то сверху
В этой версии программмы звук плоский и движется по прямой между ушами
Также не хватает глубины звука и объема"""
from pydub import AudioSegment
from time import clock

clock()  # засечь время выполнения
sound = AudioSegment.from_mp3('main.mp3')

first = 1
second = 41  # Песня измеряется в мс
# Смысл всей проги в том, что прохожусь по всей песне слайсами по несколько десятков мс
# Слайсы оч малы, и в каждом слайсе я чуть меняю направление звука
# -1 - звук полностью в левом ухе, +1 - целиком в правом
# Происходит это за счет уменьшения Дб на одном из наушников и увеличения на столько же на правом
# Из-за этого происходит эффект движения звука от уха к уху
t = -0.80  # Звук на 80% в левом ухе
reaches_1 = False  # достиг ли звук правого уха
while second < len(sound):  # до конца песни
    if not reaches_1:
        t += 0.02
        sound = sound[:first] + sound[first:second].pan(+t) + sound[second:]  # в слайсе чуть меняем звук
        # до и после слайса все оставляем как есть
    else:
        t -= 0.02
        sound = sound[:first] + sound[first:second].pan(+t) + sound[second:]
    first += 40
    second += 40  # прибавляем мс и движемся к след слайсу
    if t > 0.8: reaches_1 = True
    if t < 0.8: reaches_1 = False  # если звук почти целиком в одном из ушей - меняем направление
    # print(t)

file = sound.export('walking sound.wav', format='wav')
print(clock())
