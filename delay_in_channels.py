"""
Это первый файл и первая попытка сделать звук по-настоящему объемным
Я исходил из мысли, высказанной в интернете, что большинство якобы 8д музыки в сети
Это просто задержка в одном наушнике звука на 30-100 мс
Что я и сделал
Эффекта объема не почувствовал, на 8д с ютуба и близко не похоже
Гуглить по запросу Haas effect
"""

import librosa


def main():
    # обрезаем оригинальную дорожку для более удобной работы
    audio, sr = librosa.core.load('main.mp3', mono=False)
    librosa.output.write_wav('sample.wav', audio, sr)

    # отправляем звук целиком в левое ухо. А потом целиком в правое
    # Получилось 2 разные дорожки
    # Стерео дорожка предстввляется дыумерным массивом
    # В каждом его измерении числа, отражающие семпл рейт для своего канала
    # Занулением одной из 2 строк списка мы убрали звук из одного из каналов
    for num in range(len(audio[0])):
        audio[0][num] = 0
    librosa.output.write_wav('right_channel.wav', audio, sr)

    audio2, sr2 = librosa.core.load('sample.wav', mono=False)
    for num in range(len(audio2[0])):
        audio2[1][num] = 0
    librosa.output.write_wav('left_channel.wav', audio2, sr2)

    # задержка будет произвожиться на основе бита песни
    # загружаем снова, так как бит почему-то считается только в моно записи
    audio3, sr3 = librosa.core.load('sample.wav')
    tempo = librosa.beat.tempo(y=audio3)[0]
    del audio3, sr3

    # компоновка двух дорожек в одну
    # Соединяем 2 двумерных списка в один, не забывая один из них задержать
    n = int(round(tempo) * 20)
    # n = 2200
    for hz in range(n, len(audio[0])):
        audio[0][hz] = audio2[0][hz - n]
    librosa.output.write_wav('delay asynchronous.wav', audio, sr)
    print('done')


main()
